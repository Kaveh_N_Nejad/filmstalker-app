package kaveh.nejad.filmstalker.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import kaveh.nejad.filmstalker.models.FilmLocation;

@Dao
public interface FilmLocationDao {
    @Insert
    void insert(FilmLocation filmLocation);

    @Query("SELECT * FROM film_locations")
    List<FilmLocation> getAll();

    @Query("SELECT * FROM film_locations Where filmId = :filmId")
    List<FilmLocation> getLocationsOfFilm(int filmId);

    @Query("SELECT * FROM film_locations Where uid = :locationID")
    FilmLocation getLocation(int locationID);

    @Query("DELETE FROM film_locations Where filmId = :filmId")
    void deleteLocationsOfFilm(int filmId);

    @Update
    void update(FilmLocation filmLocation);
}
