package kaveh.nejad.filmstalker.uI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.helperClasses.ConvertBitmapStringToBitmap;
import kaveh.nejad.filmstalker.models.Film;

public class FilmItemInListBaseAdapter extends BaseAdapter {
    Context context;
    List<Map<String, String>> films;
    List<Film> filmsFromDB;
    LayoutInflater inflater;

    public FilmItemInListBaseAdapter(Context context, List<Map<String, String>> films, List<Film> filmsOnDb) {
        this.context = context;
        this.films = films;
        this.filmsFromDB = filmsOnDb;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if ( !(filmsFromDB == null) ) {
            return filmsFromDB.size();
        }

        return films.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.activity_film_item_in_list, null);
        TextView filmNameTextView = (TextView) convertView.findViewById(R.id.filmName);
        filmNameTextView.setText(name(position));
        TextView filmYearTextView = (TextView) convertView.findViewById(R.id.filmYear);
        filmYearTextView.setText(year(position));
        String filmsPosterBitmapAsString = posterBitmapAsString(position);
        ImageView posterImage = (ImageView) convertView.findViewById(R.id.posterImage);
        posterImage.setImageBitmap(( new ConvertBitmapStringToBitmap() ).StringToBitMap(filmsPosterBitmapAsString));

        return convertView;
    }

    private String name(int position) {
        if ( !(filmsFromDB == null) ) {
            return filmsFromDB.get(position).getTitle();
        }

        return films.get(position).get("film_name");
    }

    private String year(int position) {
        if ( !(filmsFromDB == null) ) {
            return String.valueOf(filmsFromDB.get(position).getYear());
        }

        return films.get(position).get("year");
    }

    private String posterBitmapAsString(int position) {
        if ( !(filmsFromDB == null) ) {
            return filmsFromDB.get(position).getPosterBitmapAsString();
        }

        return films.get(position).get("poster_bitmap_as_string");
    }
}
