package kaveh.nejad.filmstalker.voleyRequests;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Collections;

public class StringVolleyRequestWithHeaders extends StringRequest {
    private java.util.Map<String,String> headers;

    public StringVolleyRequestWithHeaders(int method, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener, java.util.Map<String,String> headers) {
        super(method, url, listener, errorListener);
        this.headers = headers;
        if (!(this.headers == null)) { return; };

        this.headers = Collections.emptyMap();
    }

    @Override
    public java.util.Map<String, String> getHeaders() throws AuthFailureError {
      return this.headers;
    }
}
