package kaveh.nejad.filmstalker.uI;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kaveh.nejad.filmstalker.R;

public class HomeScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_screen);
    }

    public void openMap(View view) {
        Intent mapsActivity = new Intent(this, MapsActivity.class);
        startActivity(mapsActivity);
    }
}