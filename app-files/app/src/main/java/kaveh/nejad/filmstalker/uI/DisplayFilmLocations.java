package kaveh.nejad.filmstalker.uI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmDao;
import kaveh.nejad.filmstalker.database.FilmLocationDao;
import kaveh.nejad.filmstalker.helperClasses.ConvertBitmapStringToBitmap;
import kaveh.nejad.filmstalker.models.Film;
import kaveh.nejad.filmstalker.models.FilmLocation;

public class DisplayFilmLocations extends AppCompatActivity {

    Film film;
    String imdbId;
    List<FilmLocation> filmLocationsList;
    AppDatabase db;
    FilmDao filmDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_film_locations);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name").build();

        setFilm();
    }

    private void setFilm(){
        Bundle extras = getIntent().getExtras();
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            filmDao = db.FilmDao();
            int filmId = Integer.parseInt(extras.getString("filmId"));
            film = filmDao.getFilmById(filmId);
            imdbId = film.getImdbId();

            FilmLocationDao filmLocationDao = db.FilmLocationDao();

            filmLocationsList = filmLocationDao.getLocationsOfFilm(filmId);
            onFilmSet();
        });
    }

    private void onFilmSet(){
        ((TextView)findViewById(R.id.filmNameOnLocations)).setText(film.getTitle());
        ((TextView)findViewById(R.id.filmYearOnLocations)).setText(String.valueOf(film.getYear()));

        String totalLocationsText = getString(R.string.total_locations, filmLocationsList.size());
        setLocationsVisitedText();


        ((TextView)findViewById(R.id.totalLocations)).setText(totalLocationsText);

        Switch shouldDisplaySwitch = (Switch) findViewById(R.id.shouldDisplaySwitch);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                shouldDisplaySwitch.setChecked(film.isShouldDisplay());
            }
        });

        shouldDisplaySwitch.setOnClickListener(view -> {
                toggleShouldDisplay(shouldDisplaySwitch);
            }
        );

        ImageView posterView = (ImageView)findViewById(R.id.posterView);
        posterView.setImageBitmap(( new ConvertBitmapStringToBitmap() ).StringToBitMap(film.getPosterBitmapAsString()));

        populateFilmLocations();
    }

    public void setLocationsVisitedText(){
        String visitedLocationsText = getString(R.string.visited_locations, visitedLocationsCount());
        ((TextView)findViewById(R.id.visitedLocations)).setText(visitedLocationsText);
    }

    private void toggleShouldDisplay(Switch shouldDisplaySwitch) {
        AppDatabase db = Room.databaseBuilder(this.getBaseContext(),
                AppDatabase.class, "database-name").build();

        ExecutorService executor = Executors.newSingleThreadExecutor();

        film.setShouldDisplay(shouldDisplaySwitch.isChecked());
        executor.execute(() -> {
            FilmDao filmDao = db.FilmDao();

            filmDao.update(film);
        });
    }

    private void populateFilmLocations() {
        DisplayFilmLocations displayFilmLocations = this;
        runOnUiThread(new Runnable() {
            public void run() {
                ListView locationList = (ListView) findViewById(R.id.locationList);
                LocationItemInListBaseAdapter locationItemInListBaseAdapter = new LocationItemInListBaseAdapter(getApplicationContext(), filmLocationsList, displayFilmLocations);
                locationList.setAdapter(locationItemInListBaseAdapter);
            }
        });
    }

    public void OnDeletePressed(View view){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        FilmLocationDao filmLocationDao = db.FilmLocationDao();

        executor.execute(() -> {
            filmLocationDao.deleteLocationsOfFilm(film.getUid());
            filmDao.delete(film);
        });

        Intent mapsActivity = new Intent(this, MapsActivity.class);
        startActivity(mapsActivity);
    }

    private int visitedLocationsCount(){
        int count = 0;

        for (FilmLocation location: filmLocationsList) {
            if (location.getHasBeenVisited()) {
                count++;
            }
        }

        return count;
    }

    public void OnOpenImdbPressed(View view){
        Uri uri = Uri.parse("https://www.imdb.com/title/" + imdbId); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}