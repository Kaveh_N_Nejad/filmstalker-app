package kaveh.nejad.filmstalker.uI;

import android.content.Context;
import android.widget.CompoundButton;

import androidx.room.Room;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmLocationDao;
import kaveh.nejad.filmstalker.models.FilmLocation;

public class OnFilmLocationSwitchChangeListener implements CompoundButton.OnCheckedChangeListener {
    FilmLocation filmLocation;
    Context context;
    DisplayFilmLocations displayFilmLocations;

    public OnFilmLocationSwitchChangeListener(FilmLocation filmLocation, Context context, DisplayFilmLocations displayFilmLocations) {
        this.filmLocation = filmLocation;
        this.context = context;
        this.displayFilmLocations = displayFilmLocations;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        filmLocation.setHasBeenVisited(isChecked);

        AppDatabase db = Room.databaseBuilder(context,
                AppDatabase.class, "database-name").build();

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            FilmLocationDao filmLocationDao = db.FilmLocationDao();

            filmLocationDao.update(filmLocation);
        });
        displayFilmLocations.setLocationsVisitedText();
    }
}
