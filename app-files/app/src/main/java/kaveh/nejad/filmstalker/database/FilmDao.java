package kaveh.nejad.filmstalker.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import kaveh.nejad.filmstalker.models.Film;

@Dao
public interface FilmDao {
    @Insert
    long insert(Film film);

    @Query("SELECT * FROM film")
    List<Film> getAll();

    @Query("SELECT * FROM film Where uid = :filmId")
    Film getFilmById(int filmId);

    @Query("SELECT * FROM film Where imdbId = :imdbId")
    Film getFilmByImdbId(String imdbId);

    @Delete
    void delete(Film film);

    @Update
    void update(Film film);
}
