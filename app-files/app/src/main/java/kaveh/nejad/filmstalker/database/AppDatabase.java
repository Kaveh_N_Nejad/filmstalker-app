package kaveh.nejad.filmstalker.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import kaveh.nejad.filmstalker.models.Film;
import kaveh.nejad.filmstalker.models.FilmLocation;

@Database(entities = {FilmLocation.class, Film.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract FilmDao FilmDao();
    public abstract FilmLocationDao FilmLocationDao();
}
