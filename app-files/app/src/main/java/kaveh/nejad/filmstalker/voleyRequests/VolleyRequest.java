package kaveh.nejad.filmstalker.voleyRequests;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.CollationElementIterator;

import kaveh.nejad.filmstalker.uI.VolleyRequesterBaseAppCompatActivity;

public class VolleyRequest {
    private VolleyRequesterBaseAppCompatActivity returnToClass;
    private String url;
    private java.util.Map<String,String> headers;

    public VolleyRequest(VolleyRequesterBaseAppCompatActivity returnToClass, String url, java.util.Map<String,String> headers) {
        this.returnToClass = returnToClass;
        this.url = url;
        this.headers = headers;
    }

    public void volleyRequest() {
        RequestQueue queue = Volley.newRequestQueue(this.returnToClass);

        Response.ErrorListener onError = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleError(error);
            }
        };

        Response.Listener<String> onSuccess = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handleSuccess(response);
            }
        };

        StringRequest stringRequest = new StringVolleyRequestWithHeaders(
                Request.Method.GET,
                url,
                onSuccess,
                onError,
                this.headers
        );

        queue.add(stringRequest);
    }

    private void handleError(VolleyError error) {
        CollationElementIterator textView;
    }

    private void handleSuccess(String response) {
        returnToClass.volleyResponse(response);
    }
}