package kaveh.nejad.filmstalker.helperClasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kaveh.nejad.filmstalker.uI.AddFilmActivity;

public class ProcessOmdbData {
    List<Map<String, String>> listOfMovies = new ArrayList<Map<String, String>>();
    int bitmapsDownloaded = 0;
    int initialMoviesCount;
    AddFilmActivity returnToClass;

    public ProcessOmdbData(AddFilmActivity returnToClass) {
        this.returnToClass = returnToClass;
    }

    public synchronized void OnImageDownloaded (String bitmapAsString, int position) {
        listOfMovies.get(position).put("poster_bitmap_as_string", bitmapAsString);
        if (!(++bitmapsDownloaded >= initialMoviesCount)) {
            return;
        }

        returnToClass.onOmdbInfoRetrived(listOfMovies);
    }

    public void GetFilmInfoFromOmdbResponse(String response) {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray filmsList = jsonResponse.getJSONArray("Search");

            initialMoviesCount = filmsList.length();
            for (int i = 0; i < filmsList.length(); i += 1) {
                Map<String,String> movie = GetFilmInfoFromOmdbElement((JSONObject) filmsList.get(i));
                listOfMovies.add(i, movie);
                (new DownloadImageAsBitmapString(movie.get("poster_url"), this, i)).download();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Map<String,String> GetFilmInfoFromOmdbElement(JSONObject jsonObject) {
        try {
            return new HashMap<String, String>() {
                {
                    put("film_name", jsonObject.getString("Title"));
                    put("imdbID", jsonObject.getString("imdbID"));
                    put("year", jsonObject.getString("Year"));
                    put("poster_url", jsonObject.getString("Poster"));
                }
            };
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
