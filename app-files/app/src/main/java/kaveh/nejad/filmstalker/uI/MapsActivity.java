package kaveh.nejad.filmstalker.uI;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.room.Room;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmDao;
import kaveh.nejad.filmstalker.database.FilmLocationDao;
import kaveh.nejad.filmstalker.databinding.ActivityMapsBinding;
import kaveh.nejad.filmstalker.helperClasses.ConvertBitmapStringToBitmap;
import kaveh.nejad.filmstalker.models.Film;
import kaveh.nejad.filmstalker.models.FilmLocation;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {
    private AppDatabase db;
    private GoogleMap googleMap;
    private FilmLocationDao filmLocationDao;
    private int currentLocationId;
    private Marker currentLocationMarker;


    private ActivityMapsBinding binding;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location userCurrentLocation;
    private Marker locationMarker;


    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private Map<Marker, HashMap> allMarkersMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name").build();
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new MapsFragment()).commit();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        initLocationListener();
        getUserCurrentLocation();
        setMarkerAtMyLocation();
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshMap();
    }

    public void toggleVisited(View view){
        executor.execute(() -> {
            filmLocationDao = db.FilmLocationDao();

            FilmLocation filmLocation = filmLocationDao.getLocation(currentLocationId);

            if (filmLocation == null) { return; }

            filmLocation.setHasBeenVisited(!filmLocation.getHasBeenVisited());
            filmLocationDao.update(filmLocation);

            FilmDao filmDao = db.FilmDao();
            Film film = filmDao.getFilmById(filmLocation.getFilmId());
            Bitmap filmPoster = new ConvertBitmapStringToBitmap().StringToBitMap(film.getPosterBitmapAsString());

            runOnUiThread(() -> {
                currentLocationMarker.remove();
                addFilmLocationToMap(filmLocation, filmPoster);
            });
        });
    }

    public void openAddMoviePage(View view) {
        Intent addFilmActivity = new Intent(this, AddFilmActivity.class);
        startActivity(addFilmActivity);
    }

    public void setMap(GoogleMap googleMap){
        this.googleMap = googleMap;
        setOnMarkerClick();
        refreshMap();
    }

    public void openMyStalks(View view) {
        Intent selectFilmActivity = new Intent(this, SelectFilmFromList.class);
        selectFilmActivity.putExtra("next_page", "myStalks");
        startActivity(selectFilmActivity);
    }

    public void refreshMap() {
        if (googleMap == null) { return; }

        googleMap.clear();
        setMarkerAtMyLocation();
        allMarkersMap = new HashMap<Marker, HashMap>();
        executor.execute(() -> {
            FilmDao filmDao = db.FilmDao();
            List<Film> films = filmDao.getAll();

            filmLocationDao = db.FilmLocationDao();
            for (Film film : films) {
                addFilmLocationsOfFilmToMap(film);
            }
        });
    }

    private void addFilmLocationsOfFilmToMap(Film film) {
        if (!film.isShouldDisplay()) { return; }
        ;
        List<FilmLocation> filmLocations = filmLocationDao.getLocationsOfFilm(film.getUid());
        for (FilmLocation filmLocation : filmLocations) {
            Bitmap filmPoster = new ConvertBitmapStringToBitmap().StringToBitMap(film.getPosterBitmapAsString());

            addFilmLocationToMap(filmLocation, filmPoster);
        }
    }

    private void addFilmLocationToMap(FilmLocation filmLocation, Bitmap filmPoster) {
        LatLng locationLatLng = getLocationFromAddress(filmLocation.getAddress());
        runOnUiThread(() -> {
            int height = 200;
            int width = 150;
            Bitmap posterMarker = Bitmap.createScaledBitmap(filmPoster, width, height, false);
            Bitmap posterMarkerWithBoarder = AddBoarderToBitmap(posterMarker, BoarderColourOfMarker(filmLocation));

            MarkerOptions markerOptions = new MarkerOptions()
                    .position(locationLatLng)
                    .title(filmLocation.getAddress())
                    .icon(BitmapDescriptorFactory.fromBitmap(posterMarkerWithBoarder));;

            Marker marker = googleMap.addMarker(markerOptions);
            allMarkersMap.put(marker, markerData(filmLocation));
        });
    }

    private HashMap<String, Integer> markerData(FilmLocation filmLocation) {
        HashMap<String, Integer> markerData = new HashMap<String, Integer>();
        markerData.put("film_id", filmLocation.getFilmId());
        markerData.put("film_location_id", filmLocation.getUid());
        return markerData;
    }

    private void setOnMarkerClick(){
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (locationMarker.equals(marker)) { return false; }
                HashMap<String, Integer> markerData = allMarkersMap.get(marker);

                currentLocationMarker = marker;
                currentLocationId = (int) allMarkersMap.get(marker).get("film_location_id");
                return false;
            }
        });
    }

    // http://www.java2s.com/example/android/graphics/add-white-border-to-bitmap.html
    public Bitmap AddBoarderToBitmap(Bitmap bitmap, int colour) {
        int borderSize = 10;
        Bitmap bmpWithBorder = Bitmap.createBitmap(bitmap.getWidth()
                        + borderSize * 2, bitmap.getHeight() + borderSize * 2,
                bitmap.getConfig());
        Canvas canvas = new Canvas(bmpWithBorder);
        canvas.drawColor(colour);
        canvas.drawBitmap(bitmap, borderSize, borderSize, null);
        return bmpWithBorder;
    }

    // Code stolen from https://stackoverflow.com/questions/24352192/android-google-maps-add-marker-by-address
    public LatLng getLocationFromAddress(String strAddress)
    {
        Geocoder coder= new Geocoder(getApplicationContext());
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 5);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return p1;
    }

    public int BoarderColourOfMarker(FilmLocation filmLocation) {
        if (filmLocation.getHasBeenVisited()) { return Color.GREEN; }

        return Color.RED;
    }

    // Location stuff mostly from https://www.c-sharpcorner.com/article/implementing-maps-activity-and-getting-user-location-in-android-studio/
    private void getUserCurrentLocation() {
        if (isPermissionGranted()) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted();
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            } else {
                userCurrentLocation=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }
    private boolean isPermissionGranted() {
        Boolean permissionGranted=true;
        String[] permissions=new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        if (ActivityCompat.checkSelfPermission(this, permissions[0])!= PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(this, permissions[1])!= PackageManager.PERMISSION_GRANTED
        ){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions,123);
            }
        }
        return permissionGranted;
    }

    private boolean locationsAreTheSame(Location location1, Location location2){
        return (location1 != null) && location2.getLatitude() == location1.getLatitude() && location2.getLatitude() == location1.getLatitude();
    }

    private void initLocationListener() {
        locationListener=new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                if (locationsAreTheSame(userCurrentLocation, location)) {
                    return;
                }
                userCurrentLocation = location;
                setMarkerAtMyLocation();
            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {
                LocationListener.super.onProviderEnabled(provider);
                Toast.makeText(MapsActivity.this, "Provider Enabled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {
                LocationListener.super.onProviderDisabled(provider);
                Toast.makeText(MapsActivity.this, "Provider disable", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                LocationListener.super.onStatusChanged(provider, status, extras);
                Toast.makeText(MapsActivity.this, "Status changed", Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setMarkerAtMyLocation();
    }

    private void setMarkerAtMyLocation(){
        getUserCurrentLocation();
        if (userCurrentLocation!=null){
            Geocoder geocoder=new Geocoder(this);
            LatLng myLocation = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
            if (locationMarker != null ) { locationMarker.remove(); }
            locationMarker = googleMap.addMarker(new MarkerOptions().position(myLocation).title("your location"));
        }
    }

    @Override
    public void onClick(View v) {
    }
}