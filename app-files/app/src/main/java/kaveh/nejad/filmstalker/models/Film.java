package kaveh.nejad.filmstalker.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "film")
public class Film {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int uid;

    private String imdbId;
    private String Title;
    private int year;
    private boolean ShouldDisplay;
    private String PosterBitmapAsString;

    public Film(String imdbId, String Title, String PosterBitmapAsString, int year) {
        this.imdbId = imdbId;
        this.Title = Title;
        this.year = year;
        this.PosterBitmapAsString = PosterBitmapAsString;
        ShouldDisplay = true;
    }

    public String getTitle() {
        return Title;
    }

    public int getYear() {
        return year;
    }

    public boolean isShouldDisplay() {
        return ShouldDisplay;
    }

    public void setShouldDisplay(boolean shouldDisplay) {
        ShouldDisplay = shouldDisplay;
    }

    public String getImdbId() {
        return imdbId;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPosterBitmapAsString() {
        return PosterBitmapAsString;
    }
}
