package kaveh.nejad.filmstalker.uI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmDao;
import kaveh.nejad.filmstalker.models.Film;

public class SelectFilmFromList extends AppCompatActivity {
    List<Map<String, String>> omdbFilmList;
    List<Film> filmsOnDB;
    ListView listView;
    String nextPage;
    AppCompatActivity selectFilmFromList;
    FilmItemInListBaseAdapter filmItemInListBaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_movie_from_list);
        SetExtras();

        selectFilmFromList = this;
        listView = (ListView) findViewById(R.id.filmListView);
        createFilmItemInListBaseAdapter();
    }

    private void OnFilmItemInListBaseAdapterCreated(){
        listView.setAdapter(filmItemInListBaseAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openNextScreen(position);
            }
        });
    }

    private void openNextScreen(int position) {
        if ( nextPage.equals("getFilmLocations") ) {
            Intent getFilmLocationsActivity = new Intent(selectFilmFromList, GetFilmLocationsActivity.class);
            getFilmLocationsActivity.putExtra("omdbFilmInfo", (Serializable) omdbFilmList.get(position));
            startActivity(getFilmLocationsActivity);
            return;
        }

        Intent displayFilmLocations = new Intent(selectFilmFromList, DisplayFilmLocations.class);
        displayFilmLocations.putExtra("filmId", String.valueOf(filmsOnDB.get(position).getUid()));
        startActivity(displayFilmLocations);
    }

    private void createFilmItemInListBaseAdapter() {
        if ( nextPage.equals("getFilmLocations") ) {
            filmItemInListBaseAdapter = new FilmItemInListBaseAdapter(getApplicationContext(), omdbFilmList, null);
            OnFilmItemInListBaseAdapterCreated();
            return;
        }
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name").build();

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            FilmDao filmDao = db.FilmDao();

            filmsOnDB = filmDao.getAll();

            filmItemInListBaseAdapter = new FilmItemInListBaseAdapter(getApplicationContext(), null, filmsOnDB);
            runOnUiThread(new Runnable() {
                public void run() {
                    OnFilmItemInListBaseAdapterCreated();
                }
            });
        });
    }

    private void SetExtras(){
        Bundle extras = getIntent().getExtras();

        omdbFilmList = (List<Map<String, String>>) extras.getSerializable("omdbFilmList");
        nextPage = extras.getString("next_page");
    }
}