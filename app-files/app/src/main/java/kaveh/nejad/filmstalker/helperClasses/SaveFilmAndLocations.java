package kaveh.nejad.filmstalker.helperClasses;

import androidx.room.Database;
import androidx.room.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmDao;
import kaveh.nejad.filmstalker.database.FilmLocationDao;
import kaveh.nejad.filmstalker.models.Film;
import kaveh.nejad.filmstalker.models.FilmLocation;

public class SaveFilmAndLocations {
    private Map<String, String> imdbInfo;
    private String imdbID;
    private AppDatabase db;

    public void call(String response, Map<String, String> imdbInfo, String imdbID, AppDatabase db){
        this.imdbInfo = imdbInfo;
        this.imdbID = imdbID;
        this.db = db;

        int film_id = storeFilm();
        storeFilmLocations(response, film_id);
    }

    private int storeFilm(){
        Film film = new Film(imdbInfo.get("imdbID"), imdbInfo.get("film_name"), imdbInfo.get("poster_bitmap_as_string"), Integer.parseInt(imdbInfo.get("year")));
        FilmDao filmDao = db.FilmDao();
        long filmId = filmDao.insert(film);
        return (int) filmId;
    }

    private void storeFilmLocations(String response, int film_id) {
        String[] locations = GetFilmLocationsFromImdbResponse(response);

        for (int i = 0; i < locations.length; i += 1) {
            FilmLocation filmLocation = new FilmLocation(film_id, locations[i]);
            FilmLocationDao filmLocationDao = db.FilmLocationDao();
            filmLocationDao.insert(filmLocation);
        }
    }

    private String[] GetFilmLocationsFromImdbResponse(String response) {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray filmsLocationsList = jsonResponse.getJSONArray("locations");

            String[] listOfLocations = new String[filmsLocationsList.length()];
            for (int i = 0; i < filmsLocationsList.length(); i += 1) {
                JSONObject filmLocation = (JSONObject) filmsLocationsList.get(i);
                listOfLocations[i] = (filmLocation.getString("location"));
            }
            return listOfLocations;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
