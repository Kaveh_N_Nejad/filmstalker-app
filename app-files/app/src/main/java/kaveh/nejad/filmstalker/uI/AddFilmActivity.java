package kaveh.nejad.filmstalker.uI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.helperClasses.ProcessOmdbData;
import kaveh.nejad.filmstalker.voleyRequests.VolleyRequest;

public class AddFilmActivity extends VolleyRequesterBaseAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_film);
    }

    public void searchForFilm(View view) {
        String url = "https://www.omdbapi.com/?apikey=7c3f362c&s=" + FilmNameToSearchFor();
        VolleyRequest volleyRequest = new VolleyRequest(this, url, null);
        volleyRequest.volleyRequest();
    }

    public String FilmNameToSearchFor() {
        TextView filmNameInputTextLocation = findViewById(R.id.filmNameInputTextLocation);
        return filmNameInputTextLocation.getText().toString();
    }

    @Override
    public void volleyResponse(String response) {
        new ProcessOmdbData(this).GetFilmInfoFromOmdbResponse(response);
    }

    public void onOmdbInfoRetrived(List<Map<String, String>> listOfMovies){
        OpenFilmSelector(listOfMovies);
    }

    private void OpenFilmSelector(List<Map<String, String>> listOfMovies) {
        Intent selectFilmActivity = new Intent(this, SelectFilmFromList.class);
        selectFilmActivity.putExtra("omdbFilmList", (Serializable) listOfMovies);
        selectFilmActivity.putExtra("next_page", "getFilmLocations");
        startActivity(selectFilmActivity);
    }
}
