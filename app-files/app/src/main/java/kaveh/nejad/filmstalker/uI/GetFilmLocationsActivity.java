package kaveh.nejad.filmstalker.uI;

import android.content.Intent;
import android.os.Bundle;

import androidx.room.Database;
import androidx.room.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.database.AppDatabase;
import kaveh.nejad.filmstalker.database.FilmDao;
import kaveh.nejad.filmstalker.database.FilmLocationDao;
import kaveh.nejad.filmstalker.helperClasses.SaveFilmAndLocations;
import kaveh.nejad.filmstalker.models.Film;
import kaveh.nejad.filmstalker.models.FilmLocation;
import kaveh.nejad.filmstalker.voleyRequests.VolleyRequest;

public class GetFilmLocationsActivity extends VolleyRequesterBaseAppCompatActivity {
    private Map<String, String> imdbInfo;
    private String imdbID;
    private AppDatabase db;
    String testingResponse = "{\"@type\":\"imdb.api.title.filminglocations\",\"base\":{\"id\":\"/title/tt0800369/\",\"image\":{\"height\":1186,\"id\":\"/title/tt0800369/images/rm3272546304\",\"url\":\"https://m.media-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_.jpg\",\"width\":800},\"runningTimeInMinutes\":115,\"title\":\"Thor\",\"titleType\":\"movie\",\"year\":2011},\"id\":\"/title/tt0800369/\",\"locations\":[{\"extras\":[\"Puente Antiguo\"],\"id\":\"/title/tt0800369/filminglocations/lc0353107\",\"interestingVotes\":{\"down\":3,\"up\":41},\"location\":\"Cerro Pelon Movie Ranch - 5547 Highway 41, Galisteo, New Mexico, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353115\",\"interestingVotes\":{\"down\":1,\"up\":20},\"location\":\"Santa Fe, New Mexico, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353110\",\"interestingVotes\":{\"down\":1,\"up\":14},\"location\":\"La Jolla, San Diego, California, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353112\",\"interestingVotes\":{\"up\":6},\"location\":\"Mystery Mesa, California, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353108\",\"interestingVotes\":{\"down\":1,\"up\":9},\"location\":\"The Getty Center - 1200 Getty Center Drive, Brentwood, Los Angeles, California, USA\"},{\"attributes\":[\"studio\"],\"id\":\"/title/tt0800369/filminglocations/lc0353114\",\"interestingVotes\":{\"down\":1,\"up\":5},\"location\":\"Raleigh Manhattan Beach Studios - 1600 Rosecrans Avenue, Manhattan Beach, California, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353109\",\"interestingVotes\":{\"down\":2,\"up\":5},\"location\":\"Granada Hills, Los Angeles, California, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353111\",\"interestingVotes\":{\"down\":3,\"up\":5},\"location\":\"Los Angeles, California, USA\"},{\"id\":\"/title/tt0800369/filminglocations/lc0353113\",\"interestingVotes\":{\"down\":3,\"up\":5},\"location\":\"New York City, New York, USA\"}]}";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_film);
        SetExtras();
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name").build();
        FilmDao filmDao = db.FilmDao();
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            if (!(filmDao.getFilmByImdbId(imdbID) == null)) {
                returnToMap();
            }
            else {
                locationRequest();
                // volleyResponse(testingResponse); // Code for testing, has the response ready, usefull as there is a max number of requests
            }
        });
    }

    private void returnToMap(){
        Intent mapsActivity = new Intent(this, MapsActivity.class);
        startActivity(mapsActivity);
    }

    private void SetExtras(){
        Bundle extras = getIntent().getExtras();
        if (extras == null) { return; }

        imdbInfo = (Map<String, String>) extras.getSerializable("omdbFilmInfo");
        imdbID = imdbInfo.get("imdbID");
    }

    @Override
    public void volleyResponse(String response) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            (new SaveFilmAndLocations()).call(response, imdbInfo, imdbID, db);
        });

        returnToMap();
    }

    public void locationRequest() {
        String url = "https://rapidapi.p.rapidapi.com/title/get-filming-locations?tconst=" + imdbID;

        java.util.Map<String, String> headers = new HashMap<String, String>()
        {
            {
                put("x-rapidapi-key", "2215b5754fmsh339dd4545346c2bp1d5792jsnc7ebe24b4e77"); // This should not be here as it is a private key, and should instead be on a server to be used as a proxy
                put("x-rapidapi-host", "imdb8.p.rapidapi.com");
            }
        };
        VolleyRequest volleyRequest = new VolleyRequest(this, url, headers);
        volleyRequest.volleyRequest();
    }
}
