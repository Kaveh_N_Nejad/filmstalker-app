package kaveh.nejad.filmstalker.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "film_locations")
public class FilmLocation {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int uid;

    private int filmId;
    private boolean hasBeenVisited;
    private String address;

    public FilmLocation(int filmId, String address) {
        this.address = address;
        this.filmId = filmId;
        hasBeenVisited = false;
    }

    public int getUid(){
        return uid;
    }

    public void setUid(int uid){
        this.uid = uid;
    }

    public boolean getHasBeenVisited(){
        return hasBeenVisited;
    }

    public void setHasBeenVisited(boolean hasBeenVisited){
        this.hasBeenVisited = hasBeenVisited;
    }

    public int getFilmId() {
        return filmId;
    }

    public String getAddress() {
        return address;
    }
}
