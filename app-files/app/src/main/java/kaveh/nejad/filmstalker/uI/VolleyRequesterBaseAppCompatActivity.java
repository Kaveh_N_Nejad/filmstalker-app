package kaveh.nejad.filmstalker.uI;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

public class VolleyRequesterBaseAppCompatActivity extends AppCompatActivity {
    public void volleyResponse(String response) {
        // This should be an exception
        System.out.println("\n\n\n\n\n\nImplement this method on child classes\n\n\n\n");
    }
}
