package kaveh.nejad.filmstalker.helperClasses;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kaveh.nejad.filmstalker.uI.AddFilmActivity;

public class DownloadImageAsBitmapString {
    private String urlString;
    private int position;
    private ProcessOmdbData returnTo;

    public DownloadImageAsBitmapString(String urlString, ProcessOmdbData  returnTo, int position) {
        this.urlString = urlString;
        this.returnTo = returnTo;
        this.position = position;
    }

    // https://stackoverflow.com/questions/14811579/how-to-create-a-custom-shaped-bitmap-marker-with-android-map-api-v2
    public void download() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        if (urlString.equals("N/A")) { returnTo.OnImageDownloaded("", position); };

        executor.execute(() -> {
            try {
                URL url = new URL(urlString);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(is);
                BitmapFactory.Options options=new BitmapFactory.Options();
                options.inSampleSize = 5;

                Bitmap bmImg = BitmapFactory.decodeStream(is, null, options);
                returnTo.OnImageDownloaded(BitMapToString(bmImg), position);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    // https://stackoverflow.com/questions/13562429/how-many-ways-to-convert-bitmap-to-string-and-vice-versa
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos =new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
