package kaveh.nejad.filmstalker.uI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import kaveh.nejad.filmstalker.R;
import kaveh.nejad.filmstalker.models.FilmLocation;

public class LocationItemInListBaseAdapter extends BaseAdapter {
    Context context;
    List<FilmLocation> locations;
    LayoutInflater inflater;
    DisplayFilmLocations displayFilmLocations;

    public LocationItemInListBaseAdapter(Context context, List<FilmLocation> locations, DisplayFilmLocations displayFilmLocations) {
        this.context = context;
        this.locations = locations;
        this.displayFilmLocations = displayFilmLocations;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.activity_location_item_in_list_activity, null);

        FilmLocation location = locations.get(position);

        TextView locationAddress = (TextView) convertView.findViewById(R.id.locationAddress);
        locationAddress.setText(location.getAddress());

        Switch visitedSwitch = (Switch) convertView.findViewById(R.id.visitedSwitch);
        visitedSwitch.setChecked(location.getHasBeenVisited());

        visitedSwitch.setOnCheckedChangeListener(new OnFilmLocationSwitchChangeListener(location, context, displayFilmLocations));

        return convertView;
    }
}
